import { ApolloServer, gql } from 'apollo-server';
import { createTestClient } from 'apollo-server-testing';

import { DataSource } from '../src';

interface DataSources {
  pingService: PingService;
}

interface Context {
  message: string;
  dataSources: DataSources;
}

class PingService extends DataSource<Context> {
  pong() {
    return `pong ${this.context.message}`;
  }
}

const server = new ApolloServer({
  typeDefs: gql`
    type Query {
      ping: String!
    }
  `,
  resolvers: {
    Query: {
      ping: (parent: any, args: any, ctx: Context, info: any) => {
        return ctx.dataSources.pingService.pong() + ' pong ' + ctx.message;
      },
    },
  },
  context: () => ({
    message: 'ping',
  }),
  dataSources: () => ({
    pingService: new PingService(),
  }),
});

describe('Query: ping', () => {
  it('should return pong', async () => {
    const { query } = createTestClient(server);
    const { data } = await query({
      query: gql`
        query Ping {
          ping
        }
      `,
    });
    expect(data!.ping).toEqual('pong ping pong ping');
  });
});
