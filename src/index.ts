import {
  DataSource as ApolloDataSource,
  DataSourceConfig,
} from 'apollo-datasource';

export class DataSource<Context = any> extends ApolloDataSource<Context> {
  protected context!: Context;

  initialize = ({ context }: DataSourceConfig<Context>) =>
    (this.context = context);
}

export default DataSource;
